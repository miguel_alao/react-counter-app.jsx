import React, { Component } from "react";
import Counter from "./counter";

class Counters extends Component {
  render() {
    console.log("counters - Rendered");
    const { onReset, counters, onDelete, onIncrement, onAdd } = this.props;

    return (
      <div>
        <button onClick={onAdd} className="btn btn-primary btn-sm m-2">
          Add
        </button>
        <button onClick={onReset} className="btn btn-danger btn-sm m-2">
          Reset
        </button>
        {counters.map(counter => (
          <Counter
            key={counter.id}
            onDelete={this.props.onDelete}
            onIncrement={this.props.onIncrement}
            counter={counter}
          />
        ))}
      </div>
    );
  }
}

export default Counters;
